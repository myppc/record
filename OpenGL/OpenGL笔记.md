# OpenGL 笔记
--------

## 章节 OpenGL基础

### 初始化openGL

+ glfwInit(); 
初始化GLFW

+ glfwWindowHint
设置glfw
参数两个为 int，第一个为类型，第二个为值

```
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);//设置glfw主版本号
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);//设置glfw次版本号
```

+ 初始化glad
gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)
GLAD是用来管理OpenGL的函数指针的，所以在调用任何OpenGL的函数之前我们需要初始化GLAD。

+ 视口
`glViewport(0, 0, 800, 600);`
glViewport函数**前两个参数控制窗口左下角的位置。第三个和第四个参数控制渲染窗口的宽度和高度（像素）**。
***glViewport 将OpenGL 坐标转换为屏幕坐标，openGL坐标为（-1,1），转换到屏幕坐标的（800，600）***


```
	//监听窗口大小变化，从新设置视口大小
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
```

+ 渲染循环
    - glfwWindowShouldClose函数在我们每次循环的开始前检查一次GLFW是否被要求退出，如果是的话该函数返回true然后渲染循环便结束了，之后为我们就可以关闭应用程序了。
    - glfwSwapBuffers交换颜色缓冲，会在这次迭代中使用buffer中的数据进行绘制
    ```
        glfwWindowShouldClose(window);//判断是否关闭窗口
        glfwSwapBuffers(window);
    ```

    **双缓冲(Double Buffer)**
    应用程序使用单缓冲绘图时可能会存在图像闪烁的问题。 这是因为生成的图像不是一下子被绘制出来的，而是按照从左到右，由上而下逐像素地绘制而成的。最终图像不是在瞬间显示给用户，而是通过一步一步生成的，这会导致渲染的结果很不真实。为了规避这些问题，我们应用双缓冲渲染窗口应用程序。前缓冲保存着最终输出的图像，它会在屏幕上显示；而所有的的渲染指令都会在后缓冲上绘制。当所有的渲染指令执行完毕后，我们交换(Swap)前缓冲和后缓冲，这样图像就立即呈显出来，之前提到的不真实感就消除了。

-------------------------------------------------------------------------

### 基础绘制

+ VAO，VBO ，EBO
    顶点数组对象：Vertex Array Object，VAO 用来存放 VBO
    顶点缓冲对象：Vertex Buffer Object，VBO 用来存放，顶点数据
    索引缓冲对象：Element Buffer Object，EBO或Index Buffer Object，IBO 用来缓存顶点索引，即三角形三个点的下标索引

+ 图形渲染管线 Graphics Pipeline
    图形渲染管线可以被划分为两个主要部分：第一部分把你的3D坐标转换为2D坐标，第二部分是把2D坐标转变为实际的有颜色的像素。

    渲染流程
    ![1](/OpenGL/Image/1.png)

    - 顶点着色器
    顶点着色器(Vertex Shader)，它把一个单独的顶点作为输入。顶点着色器主要的目的是把3D坐标转为另一种3D坐标（后面会解释），同时顶点着色器允许我们对顶点属性进行一些基本处理。
    一旦顶点坐标已经在顶点着色器中处理过，它们就应该是标准化设备坐标了，标准化设备坐标是一个x、y和z值在-1.0到1.0的一小段空间。任何落在范围外的坐标都会被丢弃/裁剪，不会显示在你的屏幕上
    - 图元装配
    将顶点着色器传入的顶点数据装配成指定的图元，如点，线，三角形
    - 几何着色器
    何着色器把图元形式的一系列顶点的集合作为输入，它可以通过产生新顶点构造出新的（或是其它的）图元来生成其他形状
    - 光栅化阶段(Rasterization Stage)
    它会把图元映射为最终屏幕上相应的像素，生成供片段着色器(Fragment Shader)使用的片段(Fragment)。在片段着色器运行之前会执行裁切(Clipping)。裁切会丢弃超出你的视图以外的所有像素，用来提升执行效率。
    - 片段着色器
    片段着色器的主要目的是计算一个像素的最终颜色，这也是所有OpenGL高级效果产生的地方。通常，片段着色器包含3D场景的数据（比如光照、阴影、光的颜色等等），这些数据可以被用来计算最终像素的颜色。
    - Alpha测试和混合
    这个阶段检测片段的对应的深度（和模板(Stencil)）值，用它们来判断这个像素是其它物体的前面还是后面，决定是否应该丢弃。这个阶段也会检查alpha值（alpha值定义了一个物体的透明度）并对物体进行混合(Blend)。

+ 代码流程
    1. 生成顶点buffer VBO
    2. 将buffer 绑定到GL_ARRAY_BUFFER中
    3. 绑定VBO对象
    4. 将数据拷贝到buffer中
    5. 生成索引缓存对象EBO
    7. 绑定EBO对象
    8. 将索引数据拷贝到EBO中
    9. 生成顶点数组对象 VAO
    10. 绑定VAO对象
    11. 生成顶点着色器，向顶点着色器注入代码，编译顶点着色器
    12. 生成片段着色器，向片段着色器注入代码，编译片段着色器
    13. 生成着色器程序
    14. 向着色器程序链接着色器片段
    15. 解释顶点数据使用方法
    15. 激活着色器程序
    16. 指定使用VAO或者EBO（再次调用绑定函数）
    17. 调用绘制方法


+ 生成顶点缓存对象，并且拷贝数据到缓存中

    ```
        //顶点的数据值
        float vertices[] = {
            -0.5f, -0.5f, 0.0f,
            0.5f, -0.5f, 0.0f,
            0.0f,  0.5f, 0.0f
        };

        unsigned int VBO;
        //生成一个顶点缓存对象
        glGenBuffers(1, &VBO);
        //将新生成的顶点缓存对象绑定到GL_ARRAY_BUFFER目标上
        glBindBuffer(GL_ARRAY_BUFFER,VBO);
        //将vertices中的数据拷贝到绑定的GL_ARRAY_BUFFER中，即VBO中
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    ```
    glBufferData第一个参数指定拷贝的目的地，第二个参数是数据大小，第三个参数是数据，第四个参数是显卡如何管理这些数据
    - GL_STATIC_DRAW ：数据不会或几乎不会改变。
    - GL_DYNAMIC_DRAW：数据会被改变很多。
    - GL_STREAM_DRAW ：数据每次绘制时都会改变。

+ 生成索引缓存对象EBO

    ```
        unsigned int EBO;
        //生成ebo对象
        glGenBuffer(1,&EBO);
        //绑定EBO对象
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
        //拷贝数据
        //param1：目标为GL_ELEMENT_ARRAY_BUFFER，param2：为索引数组长度，param3：索引数组，param4：管理方式
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
    ```
    - indices为顶点索引，当要绘制两个三角形，切两个三角形有顶点重合时，indices没三个索引标记一个三角形




+ 生成顶点数组对象 VAO
    ```
        unsigned int AVO;
        //生成AVO对象
        glGenVertexArrays(1, &VAO);
        //绑定VAO时，会将VBO和EBO都绑定进VAO
	    glBindVertexArray(VAO);
    ```

+ 生成顶点着色器
```
	//shader 代码
	const char* vertexShaderSource = "#version 330 core\n"
		"layout (location = 0) in vec3 aPos;\n"
		"void main()\n"
		"{\n"
		"   gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);\n"
		"}\0";

	//生成一个顶点处理器shader
	unsigned int vertexShader;
	vertexShader = glCreateShader(GL_VERTEX_SHADER);
	//将shader代码附加到着色器上
	glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
	//编译着色器
	glCompileShader(vertexShader);
```

+ 生成片段着色器
```

    const char *fragmentShaderSource = "#version 330 core\n"
        "out vec4 FragColor;\n"
        "void main()\n"
        "{\n"
        "   FragColor = vec4(1.0f, 0.5f, 0.2f, 1.0f);\n"
        "}\n\0";

    unsigned int fragmentShader;
    fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
    glCompileShader(fragmentShader);
```

+ 着色器程序
    完成顶点着色器和片段着色器后需要把两个着色器链接到着色器程序(Shader Program)中。
```
	/// <summary>
	/// 生成着色器程序
	/// </summary>
	/// <returns></returns>
	unsigned int shaderProgram;
	shaderProgram = glCreateProgram();

	//链接顶点着色器和片段着色器
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glLinkProgram(shaderProgram);
    //删除顶点着色器和片段着色器，在链接过后就可以删这两个着色器了
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
```

+ 解释顶点数据分割
![2](/OpenGL/Image/2.png)

```
    //第一个参数：对应顶点着色器中location = 0的 in变量
    //第二个参数：顶点属性个数，读取多指定个数，shader中参数为Vec3 ，所以需要3个值来填充
    //第三个参数：数据是float类型
    //第四个参数：否希望数据被标准化(Normalize)。设置为GL_TRUE，所有数据都会被映射到0（对于有符号型signed数据是-1）到1之间
    //第五个参数：步长，每读取一次数据，下一次读取就会向后移动步长的距离再开始读取，每个数据由三个float组成,然后接上三个float表示颜色，即为(3+3) * sizeof(float)
    //第六个参数：offset，从缓冲区哪个位子开始读取
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
    //启用顶点属性，参数是指shader中location = 0的in 参数使用上面设置的指针配置
    glEnableVertexAttribArray(0);

    //分割颜色
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);
```

+ 激活着色器程序
    在渲染循环中调用以下代码
```
    //激活着色器程序
	glUseProgram(shaderProgram);
    //若使用EBO则绑定GL_ELEMENT_ARRAY_BUFFER
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

    //若使用VAO则绑定VAObuffer
    glBindVertexArray(VAO);
    glDrawArrays(GL_TRIANGLES, 0, 3);
```

------------------------------------------

### 着色器

+ GLSL语法
    + 输入输出
    每个着色器都有输入和输出，这样才能进行数据交流和传递。GLSL定义了in和out关键字专门来实现这个目的。
    每个着色器使用这两个关键字设定输入和输出，只要一个输出变量与下一个着色器阶段的输入匹配，它就会传递下去。
        + 顶点着色器输入
            1. 顶点着色器输入是从顶点数据中获取，（VBO，VAO）
            2. 为了定义顶点数据该如何管理，我们使用location这一元数据指定输入变量
            3. 顶点着色器需要为它的输入提供一个额外的layout标识，这样我们才能把它链接到顶点数据。
        + 片段着色器输入
            1. 需要一个vec4颜色输出变量，因为片段着色器需要生成一个最终输出的颜色
            2. 如果你在片段着色器没有定义输出颜色，OpenGL会把你的物体渲染为黑色（或白色）。
        + gl_Position  这是一个vec4分量，gl_Position设置的值会成为该顶点着色器的输出。
    + 着色器数据传递
        在着色器中定义一个out 变量，在接收这个数据的着色器中定义一个 in 变量，这两个变量的**类型和名字相同**，那个这两个变量会被链接在一起。
    + uniform
        + 通过uniform，CPU可以向显卡GPU专递参数
        + 使用uniform声明的变量可以在全局中使用，由于是全局变量所以不能重复命名。
        + 这个uniform可以在着色器任意阶段被访问
        ```
            shader:
                uniform vec4 uncolor;
        ```
        ```
            cpp:
                //确定uniform位置
                int uncolorLocation = glGetUniformLocation(glProgam,"uncolor");
                glUniform4f(uncolorLocation,1.0f,1.0f,1.0f,1.0f); 
        ```
    + location 
    在顶点shader中用location标记的变量会接收VBO中的数据，但是需要使用glVertexAttribPointer进行解释，该函数的第一个参数对应location的值，最后一个参数是计算offset，表示起始偏移量

----------------------------------------------------------------------------
### 纹理

+ 纹理坐标
左下角坐标为（0,0）
右上角为（1,1）
如果是2d纹理(s,t)
如果为3d纹理(s,t,r)

+ 每个顶点就会关联着一个纹理坐标(Texture Coordinate)，之后在图形的其它片段上进行片段插值(Fragment Interpolation)。

+ 采样
使用纹理坐标获取纹理颜色叫做采样(Sampling)

+ 纹理环绕方式
**GL_REPEAT**	对纹理的默认行为。重复纹理图像。
**GL_MIRRORED_REPEAT**	和GL_REPEAT一样，但每次重复图片是镜像放置的。
**GL_CLAMP_TO_EDGE**	纹理坐标会被约束在0到1之间，超出的部分会重复纹理坐标的边缘，产生一种边缘被拉伸的效果。
**GL_CLAMP_TO_BORDER**	超出的坐标为用户指定的边缘颜色。
```
    //设置指定坐标轴的纹理环绕方式
    //param1 :指定纹理类型为2d纹理
    //param2 ：指定设置的坐标轴s轴
    //param3 ：指定环绕方式
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
    //如果第二个参数设置为GL_TEXTURE_BORDER_COLOR，则需要调用glTexParameterfv
    float borderColor[] = { 1.0f, 1.0f, 0.0f, 1.0f };
    //第三个参数为一个颜色值
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
```

+ 纹理过滤
纹理过滤是指进行插值时色值采样方式
主要纹理过滤方式有两种
    + GL_NEAREST 邻近过滤 
        使用该纹理坐标对应最近的像素点进行采样
        ![3](/OpenGL/Image/3.png)
        这种方式没有经过平滑处理会出现像素点
    + GL_LINEAR 线性过滤
        对该纹理坐标附近的像素进行权重分配获取色值
        ![4](/OpenGL/Image/4.png)
        这种方式会使用平滑处理，但是纹理会模糊
![5](/OpenGL/Image/5.png)

    ```
        //设置纹理缩小时使用GL_NEAREST
        //GL_TEXTURE_MIN_FILTER 缩小
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        //设置纹理放大时使用GL_NEAREST
        //GL_TEXTURE_MAG_FILTER 放大
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    ```

+ 多级渐远纹理(Mipmap)
多级渐远纹理也可以使用glTexParameteri设置纹理过滤
**GL_NEAREST_MIPMAP_NEAREST**	使用最邻近的多级渐远纹理来匹配像素大小，并使用邻近插值进行纹理采样
**GL_LINEAR_MIPMAP_NEAREST**	使用最邻近的多级渐远纹理级别，并使用线性插值进行采样
**GL_NEAREST_MIPMAP_LINEAR**	在两个最匹配像素大小的多级渐远纹理之间进行线性插值，使用邻近插值进行采样
**GL_LINEAR_MIPMAP_LINEAR**	在两个邻近的多级渐远纹理之间使用线性插值，并使用线性插值进行采样
    ```
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    ```
+ 生成纹理
    生成一个纹理的步骤
    + 生成纹理
    + 绑定纹理
    + 为当前绑定的纹理对象设置环绕、过滤方式
    + 加载图片数据
    + 将图像数据附加到纹理中    
    + 根据情况是否调用设置多级渐远处理
    + 释放图像数据

    ```
        //图片资源的宽高，以及通道数量，这三个参数会被stbi_load设置
        int width,height,channel;
        //加载纹理数据，由第三方库加载
        unsigned char* data = stbi_load(path,&width,&height,&channel,0);
        //生成一个纹理
        unsigned int texture;
        //param1:纹理个数
        glGenTexture(1,&texture);
        //绑定纹理
        //param1:纹理类型为2d
        //param2:纹理id
        glBindTexture(GL_TEXTURE_2D,texture);
        // 为当前绑定的纹理对象设置环绕、过滤方式
        //param1:纹理类型，这里设置的是2d纹理
        //param2，param3：设置轴的环绕为GL_REPEAT
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
        // 为当前绑定的纹理对象设置过滤方式为GL_LINEAR
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        //param1：指定设置纹理类型为2d纹理
        //param2:指定多级渐远级别，0位基础级别
        //param3: OpenGL保存纹理类型，指定保存为RGB
        //param4,param5:纹理宽高
        //pram6:固定设置成0
        //param7，param8：指定传入纹理数据的方式，GL_RGB指传入的是三个色值，GL_UNSIGNED_BYTE指使用无符号byte数组传入
        //param9:图像数据
        glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,width,height,0,GL_RGB,GL_UNSIGNED_BYTE,data)
        //如果需要用到多级渐远则需要调用glGenerateMipmap
        glGenerateMipmap(GL_TEXTURE_2D);
        //释放图像数据
        stbi_image_free(data);
    ```
+ 顶点着色器于片段着色器
    顶点着色器需要接收对应纹理坐标数据，这个数据会在vbo中传入，需要新的location接收
    vexterShader
    ```
        [接收顶点数据以及顶点颜色]
        layout(location  = 2) in vec2 aTexCoord;//这个是纹理坐标

        out vec2 TexCoord;//需要把接收到的纹理坐标传送到片段着色器
        void main()
        {
            TexCoord = aTexCoord;
        }
    ```
    fragmentShader
    ```
        in vec2 TexCoord;//接收纹理坐标
        out vec4 FragColor;//输出的颜色

        uniform sampler2D ourTexture;//smapler2D 是一个纹理采样器，uniform代表外部传入

        void main()
        {
            //texture()是openGL提供的函数，用于取纹理颜色，参数1为外部传入的纹理采集器，参数2位纹理坐标
            //通过纹理坐标读取纹理中的颜色
            FragColor = texture(ourTexture,TexCoord);
        }
    ```
+ 循环渲染部分
可以将纹理通过uniform传入到片段着色器中，通过采样器获取纹理并使用
```
    //定义一个纹理位置
    int texture_pos = 0;
    //将这个纹理位置转入shader的采样器
    glUniform1i(glGetUniformLocation(shaderID, 'ourTexture'), texture_pos);
    //激活0号位置的纹理，这个位置对应的是texture_pos
    glActiveTexture(GL_TEXTURE0);
    //绑定纹理到刚才激活的位置上
    glBindTexture(GL_TEXTURE_2D,texure1);
    //激活1号位置的纹理
    glActiveTexture(GL_TEXTURE1);
    //绑定纹理
    glBindTexture(GL_TEXTURE_2D,texure2);
    //绑定顶点数组对象
    glBindVertexArray(VAO);
    
```    

------------------------------

### 坐标系
**OpenGL希望在每次顶点着色器运行后，我们可见的所有顶点都为标准化设备坐标(Normalized Device Coordinate, NDC)。通常会自己设定一个坐标的范围，之后再在顶点着色器中将这些坐标变换为标准化设备坐标。然后将这些标准化设备坐标传入光栅器(Rasterizer)，将它们变换为屏幕上的二维坐标或像素。**


![6](/OpenGL/Image/6.png)

#### 概述
从局部空间到屏幕空间需要用到三个主要的变换矩阵，我们的顶点坐标起始于局部空间(Local Space)，在这里它称为局部坐标(Local Coordinate)，它在之后会变为世界坐标(World Coordinate)，观察坐标(View Coordinate)，裁剪坐标(Clip Coordinate)，并最后以屏幕坐标(Screen Coordinate)的形式结束。
+ 模型(Model) 是将局部坐标转换到世界坐标中
+ 观察(View) 将世界坐标转换为观察坐标
+ 投影(Projection) 将观察坐标转换为裁剪坐标

+ 局部空间(Local Space，或者称为物体空间(Object Space))
+ 世界空间(World Space)
+ 观察空间(View Space，或者称为视觉空间(Eye Space))
+ 裁剪空间(Clip Space)
+ 屏幕空间(Screen Space)
--------------------------------------------------------------------
+ 裁剪空间 
    ![7](/OpenGL/Image/7.png)
    平截头体定义了可见的坐标，它由由宽、高、近(Near)平面和远(Far)平面所指定。

    + 如果只是图元(Primitive)，例如三角形，的一部分超出了裁剪体积(Clipping Volume)，则OpenGL会重新构建这个三角形为一个或多个三角形让其能够适合这个裁剪范围。
    + 平截头体(Frustum)
        每个出现在平截头体范围内的坐标都会最终出现在用户的屏幕上。将特定范围内的坐标转化到标准化设备坐标系的过程（而且它很容易被映射到2D观察空间坐标）被称之为投影(Projection)，因为使用投影矩阵能将3D坐标投影(Project)到很容易映射到2D的标准化设备坐标系中。
    + 透视除法
        个过程中我们将位置向量的x，y，z分量分别除以向量的齐次w分量；透视除法是将4D裁剪空间坐标变换为3D标准化设备坐标的过程。这一步会在每一个顶点着色器运行的最后被自动执行。

    + 正交矩阵

        ```
            //创建一个正交矩阵
            //param1:左坐标
            //param2:右坐标
            //param3:底部坐标
            //param4:顶部坐标
            //param5:近平面距离
            //param6:远平面距离
            glm::ortho(0.0f, 800.0f, 0.0f, 600.0f, 0.1f, 100.0f);
        ```
    + 透视矩阵
        ```
        //param1:fov 视野(Field of View)，
        //param2:宽高比
        //param3:近平面距离
        //param4:远平面距离
        glm::perspective(glm::radians(45.0f), (float)width/(float)height, 0.1f, 100.0f);
        ```

    + 深度测试
        **打开深度测试后会出现遮挡效果**
        ```
            //开启深度测试
        	glEnable(GL_DEPTH_TEST);
            //如果开启深度测试,则需要清理深度缓存
            glClear(GL_COLOR_BUFFER_BIT| GL_DEPTH_BUFFER_BIT);
        ```
    + 顶点shader
        ```
            uniform mat4 model;//模型矩阵
            uniform mat4 view;//观察矩阵
            uniform mat4 projection;//投影矩阵
            
            void main()
            {
                gl_Position = model * view *model * vec4;
            }
        ```
--------------------------------------------------

### 摄像机
+ LookAt
    ```
        //定义一个摄像机位子
        glm::vec3 cameraPos = glm::vec3(0.0f, 0.0f, 3.0f);

        //目标位置
        glm::vec3 cameraTarget = glm::vec3(0.0f, 0.0f, 0.0f);

        //相机方向
        glm::vec3 cameraDirection = glm::normalize(cameraPos - cameraTarget);

        //一个向上的向量于相机朝着的方向叉乘，可以获得相机的右方向
        //叉乘可以获得一个法线向量，这个向量垂直于up和cameraDir形成的平面
		glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);
		glm::vec3 cameraRight = glm::normalize(glm::cross(up, cameraDirection));

        //相机的右方向和相机朝向做叉乘就可以获得相机的up方向
        glm::vec3 cameraUP = glm::normalize(glm::cross(cameraDirection,cameraRight));

        [带入lookAt矩阵]
    ```
    ![8](/OpenGL/Image/8.png)
    其中R是右向量，U是上向量，D是方向向量P是摄像机位置向量

+ glm提供的lookat
    ```
    //param1:摄像机位置
    //param2:目标位置
    //param3：世界空间中的上方向
    glm::mat4 view;
    view = glm::lookAt(glm::vec3(0.0f, 0.0f, 3.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));

    ```

--------------------------------------------------------
### 多个shader
+ 同时使用多个shader
在渲染循环中分别使用多个shader需要在调用glUseProgram(shaderID);后为这个shader的uniform设置值，并且绑定对应的AVO，若这个shader没有启用，则设置的uniform无效


--------------------------------------------------------



## 章节2 光照

### 基础光照
+ 实现原理是利用光的反射，眼睛看到一个物体的颜色是这个物体反射的自然光，那么影响这个物体呈现的颜色有两个因素一个是物体反射光和光照颜色，将这两个光照分量相乘就可以获得基础的光照颜色了
片段着色器

    ```
    out vec4 frag;
    uniform vec3 light;//光照颜色
    uniform vec3 obj;//物体反射颜色

    void  main()
    {
        //将光照颜色和物体反射的颜色各色值分量相乘就可以得到一个简单的光照颜色了
        frag = vec4(light * obj,1.0f);

    }

    ```
-------------------------------------------------------------------

### 环境光照
要让一个物体在光照下显得真实，需要用到环境光影响，漫反射，镜面反射
+ 环境光
可以理解为在真实环境下，总是存在一定光源，这个光源保证了物体每个面都有一个很微弱的颜色，这个值可以叠加到反射光中
    ```
    float ambientStrength = 0.1f;
    vec3 ambientColor = ambient * lightColor * objectColor;
    ```

+ 漫反射
    + 漫反射原理
    ![9](/OpenGL/Image/9.png)
    当θ角越大时光线越平行于平面，则漫反射值越小，对物体的颜色影响就越小，当光纤垂直于平面时，光线对物体影响最大
    ```
        in vec3 Normal;//顶点法向量
        in vec3 lightPos;//光源位置
        in vec3 WorldPos;//顶点的世界位子

        vec3 norm = normalize(Normal);//法向量的单位向量
        vec3 lightDir = normalize(lightPos - WorldPos);//一个指向光源的向量，这个地方一定要用单位向量

        float diff = max(dot(Normal,lightDir),0);//一旦点乘的夹角出现负数，说明光照没有出现在这个面上，那么漫反射对这个面的影响为0

        vec3 diffColor = diff* lightColor;
        vec3 color = diffColor * objectColor;
    ```

    + 法向量矩阵
    当模型出现拉伸或变形时，法向量会改变需要引入一个法向量矩阵进行计算，法向量矩阵是一个mat3，计算方法为模型矩阵左上角的逆矩阵的转置矩阵
    ```
        //inverse求矩阵的逆矩阵
        //transpose求矩阵的转置矩阵
        Normal = mat3(transpose(inverse(model))) * aNormal;
    ```
    **逆矩阵消耗比较大，所以求法向量矩阵时最好放在cpu进行计算**

+ 镜面反射
    ![10](/OpenGL/Image/10.png)
    + 镜面反射原理
    当观察视线与反射光线所呈角度越小，光线的强度就越大
    R为反射光线
    ```
        in vec3 Normal;//顶点法向量
        in vec3 lightPos;//光源位置
        in vec3 WorldPos;//顶点的世界位子
        in vec3 ViewPos;//观察者位置

        vec3 lightDir = normalize(lightPos - WorldPos);//一个指向光源的向量，这个地方一定要用单位向量
        //reflect 返回一个反射向量vec3
        //第一个参数为光源到顶点的向量，取反是因为lightDir为顶点指向光源
        vec3 reflectDir = normalize(reflect(-lightDir, norm));
        vec3 viewDir = normalize(ViewPos)
        //反射向量与观察向量的夹角
        //这个角越大，那么对光的影响就越小
        vec3 refleValue = max(dot(viewDir,reflectDir),0);
        //refleValue 小于1，取幂会越来越小，那么对镜面反射的影响就越小
        float spec = pow(refleValue,32);
        vec3 specular = spec * lightColor;
    ```

+ 合并环境光，漫反射，镜面反射
    ```
        fragColor = vec4((specular + diffColor + ambientColor) * objColor);
    ``` 

+ 关于反射向量计算
[反射向量具体计算方式](https://zhuanlan.zhihu.com/p/152561125)

------------------------------------------------------------------------------
### 材质
物体的颜色在没有材质时使用一个vec3来代替rgb值，这个色值可以使用材质来替换
材质包含了这个物体在光照下对环境光的反应，对漫反射的反应和镜面反射的反应。
当我们使用这几个属性来替换物体颜色这个vec3时，可以得到更好的效果。
```
struct Material mat
{
    vec3 ambient;//物体对环境光的rgb值的反应程度
    vec3 diffuse;//物体对漫反射的rgb值的反应程度
    vec3 specular;//物体对镜面反射的rgb值的反应程度
    float shininess;//散射程度，这个值越大越趋近于金属材质
};

void main()
{
    //环境光色值
    vec3 ambient = mat.ambient * lightColor;
    
    vec3 lightDir = normalize(lightPos - WorldPos)
    float diff = max(dot(normal,lightDir),0);
    //漫反射色值
    vec3 diffuse = mat.diffuse * diff * lightColor;

    vec3 refleDir = reflect(-lightDir,normal);
    vec3 viewDir = normalize(cameraPos - WorldPos);
    //镜面反射曝光度
    vec3 spec = pow(max(dot(refleDir,viewDir),0),mat.shininess);
    //镜面反射色值
    vec3 specular = mat.specular * spec * lightColor;

    //合并三个光的色值
    vec3 result = ambient + diffuse + specular;
    FragColor = vec4(result, 1.0);
}

```

同理，我们也可以给光源定义这三个属性来代替光的vec3
```
struct Light light
{
    vec3 position;//光源位置
    vec3 ambient;//光源的环境强度
    vec3 diffuse;//光源的漫反射强度
    vec3 specular;//光源的镜面反射强度
};

struct Material mat
{
    vec3 ambient;//物体对环境光的rgb值的反应程度
    vec3 diffuse;//物体对漫反射的rgb值的反应程度
    vec3 specular;//物体对镜面反射的rgb值的反应程度
    float shininess;//散射程度，这个值越大越趋近于金属材质
};
//那么计算物体颜色时就不用再用材质的属性于光的vec3相乘，而是于光的对应属性相乘

void main()
{
    //环境光色值
    vec3 ambient = mat.ambient * light.ambient;
    //漫反射色值
    vec3 diffuse = mat.diffuse * diff * light.diffuse;
    //镜面反射色值
    vec3 specular = mat.specular * spec * light.specular;

    //合并三个光的色值
    vec3 result = ambient + diffuse + specular;
    FragColor = vec4(result, 1.0);
}
```
**这个时候一个物体呈现的颜色是受到光和物体材质的三个属性同时影响**



------------------------------------------------------------------------------
### 光照纹理

+ 使用光照纹理
在材质中带入纹理
```
    GLuint textureID ;
    //设置纹理位置
    glActiveTexture(GL_TEXTURE0);
    glGenTexture(1,&textureID);
    [设置纹理环绕方式和过滤方式]
    //绑定纹理
    glBindTexture(GL_TEXTURE_2D,textureID);
```
将纹理位置传入shader的材质结构体
glUniform1i(glGetUniformLocation(shaderID,"texture"),texture_pos);

+ 在片段着色器中使用纹理代替之前材质中环境光照分量，漫反射分量以及镜面反射分量
```
    vec3 norm = normalize(Normal);

    //使用环境光照与纹理颜色结合
	vec3 ambientV3 = vec3(texture(mat.texture1, TexCoord)) * light.ambient;

    //texture2是金属边框纹理图
    //使用texture2的纹理与镜面反射结合,这样得到的结果是只有金属边框进行镜面反射
	vec3 lightDir = normalize(lightPos - WorldPos);
	vec3 viewDir = normalize(viewPos - WorldPos);
	vec3 reflectDir = reflection(-lightDir, norm);
	float spec = pow(max(0,dot(reflectDir,viewDir)),mat.shininess);
	vec3 specular = vec3(texture(mat.texture2, TexCoord)) * spec * light.specular;

    //使用漫反射与纹理结合
	float diff = max(dot(lightDir,norm),0);
	vec3 diffuse = diff * vec3(texture(mat.texture1, TexCoord)) * light.diffuse;

	FragColor = vec4(diffuse + ambientV3 + specular ,1.0);
```

------------------------------------------------------------------------------
### 深度测试
+ 深度测试 
当深度测试(Depth Testing)被启用的时候，OpenGL会将一个片段的深度值与深度缓冲的内容进行对比。OpenGL会执行一个深度测试，如果这个测试通过了的话，深度缓冲将会更新为新的深度值。如果深度测试失败了，片段将会被丢弃。

深度缓冲是在片段着色器运行之后（以及模板测试(Stencil Testing)运行之后）在屏幕空间中运行的。屏幕空间坐标与通过OpenGL的glViewport所定义的视口密切相关，并且可以直接使用GLSL内建变量gl_FragCoord从片段着色器中直接访问。gl_FragCoord的x和y分量代表了片段的屏幕空间坐标（其中(0, 0)位于左下角）。gl_FragCoord中也包含了一个z分量，它包含了片段真正的深度值。z值就是需要与深度缓冲内容所对比的那个值。
```
    //打开深度测试
    glEnable(GL_DEPTH_TEST); 
    //设置比较方法，GL_LESS代表深度越小的越靠前
    glDepthFunc(GL_LESS);
    //清理深度测试缓存，如果每次绘制前不清理，缓存则会被保留下来
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
```



+ 深度值精度
    深度计算方法精度并不是线性的，而是越远处深度值变化越小，对于越近的地方深度值变化越大
    深度值计算公式
    ![12](/OpenGL/Image/12.png)


    ![11](/OpenGL/Image/11.png)

+ 深度冲突
    当两个面贴在一起的时候，就容易出现深度冲突，因为渲染的时候深度值计算出来一样，就会导致渲染出现花纹
    ![13](/OpenGL/Image/13.png)
    这个现象在距离越远的时候越明显，因为距离越远，两个面的深度值就越靠近
    深度缓冲是由窗口系统自动创建的，它会以16、24或32位float的形式储存它的深度值。在大部分的系统中，深度缓冲的精度都是24位的。
    如果出现深度冲突也可以提高深度缓冲的精度，24位变为32位。

----------------------------------------------------------------------------
### 模板测试
模板测试位于深度测试之前，如果没有通过模板测试则不会进行深度测试
一个模板值是8位，也就是可以有2^8(256)种不同的模板值
```
    //如果需要使用模板测试则需要调用打开测试
    glEnable(GL_STENCIL_TEST);

    //渲染之前同样需要清理缓存，否则会保留上一次的模板缓存
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    glStencilMask(0xFF); //参数为0xff时可以理解为允许模板写入,这个时候如果通过模板测试，则会调用相应的处理
    glStencilMask(0x00); //参数为0x00时可以理解为禁止模板写入,即使通过模板测试，一样不会修改模板缓存

    //设置判断方式
    //param1：GL_NEVER、GL_LESS、GL_LEQUAL、GL_GREATER、GL_GEQUAL、GL_EQUAL、GL_NOTEQUAL和GL_ALWAYS，指定判断方式
    //ref是指参考值，例如 func设置为GL_EQUAL时，指现有模板和ref相同时通过测试，这个ref可以指定任意的值(0-256)，这样可以实现有的模板值显示，而有的不显示
    //mask:掩码值 mask 和参考值 ref 值先做与操作，再把当前模板中的值 stencil 与掩码值 mask 做与操作，然后参考 func 中的方法是否可以通过
    glStencilFunc(GLenum func, GLint ref, GLuint mask)
    //指定模板测试深度测试没有通过处理方式
    //sfail：模板测试失败时采取的行为。
    //dpfail：模板测试通过，但深度测试失败时采取的行为。
    //dppass：模板测试和深度测试都通过时采取的行为。
    glStencilOp(GLenum sfail, GLenum dpfail, GLenum dppass);
```
![14](/OpenGL/Image/14.png)


+ 绘制轮廓思路
    1. 打开深度测试，打开深度写入
    2. 设置深度测试处理方式
    3. 设置深度测试判断为全部通过判断
    4. 绘制物体,这样绘制的图形会保留当前的模板值
    5. 关闭深度写入
    6. 设置深度测试判断为不等于模板时通过测试
    7. 将模型放大并绘制，这时放大的外边框在测试中得到的结果就是不等于模板的结果，然后进行绘制


------------------------------------------------------------------------------
### 颜色混合
颜色混合具体计算不需要shader来处理，由openGL完成，我们只需要设置我们需要的混合模式

**一般情况下使用这种混合方式**
混合公式
![15](/OpenGL/Image/15.png)
也可以通过参数调整混合方式
```
    glBlendEquation(GLenum mode)
```
GL_FUNC_ADD：默认选项，将两个分量相加：C¯result=Src+Dst。
GL_FUNC_SUBTRACT：将两个分量相减： C¯result=Src−Dst。
GL_FUNC_REVERSE_SUBTRACT：将两个分量相减，但顺序相反：C¯result=Dst−Src。

```
    //打开颜色混合
    glEnable(GL_BLEND);
    //函数接受两个参数，来设置源和目标因子。
    glBlendFunc(GLenum sfactor, GLenum dfactor)
```
glBlendFunc参数如下
![16](/OpenGL/Image/16.png)

**在同时使用深度测试和混合时，透明像素也会被写入到深度缓存中，就可能出现透明图片挡住视线后面的部分，所以需要对带有透明的图片进行排序，先绘制远的图片再绘制近的图片**

------------------------------------------------------------------------------
### 面剔除
openGL通过检查正面是否正对着观察者来实现面剔除，可以有效提高绘制效率
默认情况下是关闭面剔除的
```
    //打开面提出
    glEnable(GL_CULL_FACE);
```

默认情况下是剔除背对观察者的面,一般情况下逆时针旋转的顶点组成的面是正三角形，也就是要渲染的面,也可以设置顺时针旋转的顶点为正三角形
```
    //设置逆时针顶点组成的面为正三角形
    glFrontFace(GL_CCW);
    
```
GL_CCW :逆时针
GL_CW：顺时针

```
    //指定剔除正面还是反面，一般情况下设置为剔除反面
    glCullFace(GL_BACK);
```
GL_BACK：只剔除背向面。
GL_FRONT：只剔除正向面。
GL_FRONT_AND_BACK：剔除正向面和背向面。

-----------------------------------------------------------------------------
### 帧缓冲
openGL会自动生成一个帧缓冲，一般绘制时默认使用这个缓冲

可以同时存在多个帧缓冲
```
    //生成一个帧缓冲
    glGenFramebuffers(1,&fbo);
    //绑定当前生成的帧缓冲，绘制的时候会绘制到当前绑定的帧缓冲上
    glBindFramebuffer(GL_FRAMEBUFFER,fbo);
    //为这个帧缓冲附加一个纹理，当缓冲被写入时，会写入到纹理中
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fbo_texture, 0);
```
当绘制的内容有用到深度测试的时候，那么需要为新生成的framebuffer添加一个rbo，这个会用于写入缓冲，如果没有附加rbo的话就会出现错位,模板同理

```
    unsigned int rbo;
    //生成rbo
    glGenRenderbuffers(1, &rbo);
    //绑定一个rbo
    glBindRenderbuffer(GL_RENDERBUFFER, rbo);
    //可以理解为想fbo中写入需要缓存的数据
    //GL_DEPTH24_STENCIL8 ：24的深度+8位的模板
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, 800, 600);
    //添加附件
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo);
```

在绘制部分需要先向生成的fbo绘制，清除缓存也要在绑定fbo后调用clear,然后再讲fbo中绑定的纹理绘制到默认的帧缓冲中来显示
```
    glBindFramebuffer(GL_FRAMEBUFFER,fbo);
    //根据这个fbo具体用到的颜色或者深度进行清理
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    //进行正常的渲染,这部分的渲染结果会放进绑定在fbo中的纹理附件
    draw()
    //使用默认的fbo然后进行纹理绘制，同样需要清理这个fbo的帧缓冲
    glBindFramebuffer(GL_FRAMEBUFFER,0);
    glClear(GL_COLOR_BUFFER_BIT);
    //绘制绑定这fbo中的texture
    draw()
```
------------------------------------------------------------------
### GLSL
#### GLSL的内建变量
**gl_FragCoord** 图元的屏幕xyz分别代表坐标和图元深度
**gl_Position** 顶点着色器的裁剪空间输出位置向量
**gl_PointSize** gl_PointSize输出变量，它是一个float变量，你可以使用它来设置点的宽高（像素）。在顶点着色器中修改点的大小的话，你就能对每个顶点设置不同的值了。
使用gl_PointSize需要打开glEnable(GL_PROGRAM_POINT_SIZE);
**gl_FrontFacing** bool值，代表这个面是否为正面

#### 接口块
可以理解为用结构体的形式向下一个着色器传递参数
```
//shader1 :
out Mat
{
    vec3 p1;
    vec4 p2;
} mat;

//shader2 :
in Mat 
{
    vec3 p1;
    vec4 p2;
} mat;
```