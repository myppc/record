﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Extension;

namespace Assets.RainCode.Scripts.Purchasing {

	/// <summary>
	/// 初始化IAPManager的商品信息
	/// </summary>
	public class IAPProduct {
		public string ProductID;
		public ProductType ProdType;
	}

	public class IAPManager : IDetailedStoreListener {
		private static IAPManager Instance;
		private IStoreController controller;
		private IExtensionProvider extensions;

		private Action<PurchaseFailureDescription> onPurchFailure;
		private Action<PurchaseEventArgs> onPurchSucc;
		private Action<bool> onInitResult;


		public IAPManager() {
		}

		static public IAPManager GetInstance() {
			if (Instance == null) {
				Instance = new IAPManager();
			}
			return Instance;
		}


		public void Init(List<IAPProduct> products,Action<bool> onInitResult, Action<PurchaseEventArgs> onPurchSucc,Action<PurchaseFailureDescription> onPurchFailure) {
			this.onInitResult = onInitResult;
			this.onPurchFailure = onPurchFailure;
			this.onPurchSucc = onPurchSucc;
			var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
			foreach (var item in products) { 
				builder.AddProduct(item.ProductID, item.ProdType);
			}
			UnityPurchasing.Initialize(this, builder);
		}

		/// <summary>
		/// Called when Unity IAP is ready to make purchases.
		/// 初始化成功回调
		/// </summary>
		public void OnInitialized(IStoreController controller, IExtensionProvider extensions) {
			this.controller = controller;
			this.extensions = extensions;

			extensions.GetExtension<IAppleExtensions>().RestoreTransactions((result,id) => {
				if (result) {
					// 这并不意味着已恢复任何对象，
					// 只表示恢复过程成功了。
				} else {
					//恢复操作已失败。
				}
			});

			Debug.LogWarning($"IAP 初始化成功");
			onInitResult?.Invoke(true);
		}

		/// <summary>
		/// Called when Unity IAP encounters an unrecoverable initialization error.
		///
		/// Note that this will not be called if Internet is unavailable; Unity IAP
		/// will attempt initialization until it becomes available.
		/// 初始化失败回调
		/// </summary>
		public void OnInitializeFailed(InitializationFailureReason error) {
			Debug.LogWarning($"IAP 初始化失败:{error.ToString()}");
			onInitResult?.Invoke(false);
		}

		/// <summary>
		/// Called when a purchase completes.
		///
		/// May be called at any time after OnInitialized().
		/// 支付完成回调
		/// </summary>
		public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e) {
			onPurchSucc?.Invoke(e);
			return PurchaseProcessingResult.Pending;
		}

		/// <summary>
		/// 初始化失败
		/// </summary>
		/// <param name="error"></param>
		/// <param name="message"></param>
		public void OnInitializeFailed(InitializationFailureReason error, string message) {
			Debug.LogWarning($"IAP 初始化失败:{message}");
			onInitResult?.Invoke(true);
		}

		/// <summary>
		/// 支付失败回调
		/// </summary>
		/// <param name="product"></param>
		/// <param name="failureDescription"></param>
		public void OnPurchaseFailed(Product product, PurchaseFailureDescription failureDescription) {
			Debug.LogWarning($"IAP 支付失败 1 :{failureDescription.reason}");

			onPurchFailure?.Invoke(failureDescription);
		}


		/// <summary>
		/// 支付失败回调
		/// </summary>
		/// <param name="product"></param>
		/// <param name="failureReason"></param>
		public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason) {
			Debug.LogWarning($"IAP 支付失败 2 :{failureReason}");
		}


		/// <summary>
		/// 拉起调用
		/// </summary>
		/// <param name="productID"></param>
		public void InitiatePurchase(string productID) {
			Debug.LogWarning($"IAP 触发支付:{productID}");
			this.controller.InitiatePurchase(productID);

		}

		/// <summary>
		/// 确认订单完成
		/// </summary>
		/// <param name="productID"></param>
		public void ConfirmPendingPurchase(string productID) {
			var product = GetProduct(productID);
			if (product != null) {			
				this.controller.ConfirmPendingPurchase(product);
				Debug.LogWarning($"IAP 支付订单完成:{productID}");
			}
		}


		/// <summary>
		/// 获取指定的商品
		/// </summary>
		/// <param name="productID"></param>
		/// <returns></returns>
		private Product GetProduct(string productID) {
			var product = controller.products.WithID(productID);
			return product;
		}
	}
}


