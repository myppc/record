local TuaMap = {}

local startIndex,endIndex

local NodePos = 
{
	[1] = Vector2(-12,232),
	[2] = Vector2(-227,100),
	[3] = Vector2(138,106),
	[4] = Vector2(326,175),
	[5] = Vector2(-129,-48),
	[6] = Vector2(452,-85),
	[7] = Vector2(-264,-216),
	[8] = Vector2(245,-216),
	[9] = Vector2(822,-216),
	[10] = Vector2(-71,-436),
	[11] = Vector2(481,-552),
	[12] = Vector2(954,-636),
	[13] = Vector2(157,-780),
}

local map = 
{
	[1] = {2,4,5},
	[2] = {1,5},
	[3] = {4,6},
	[4] = {1,3},
	[5] = {1,2,6,7},
	[6] = {3,5,8,11},
	[7] = {5,10},
	[8] = {6,9,13},
	[9] = {8},
	[10] = {7,13},
	[11] = {6,12},
	[12] = {11,13},
	[13] = {8,10,12}

}

local Ant = {
	path = {},
	cur = nil,
	dest = nil,
	maxStep = 100,
	tua = 20
}


local Aoc = 
{
	startIndex = nil,
	endIndex = nil,
	antCount = 100,
	antList = {},
	rho = 0.2,
	baseTua = 20
}


function Aoc:InitTuaMap()
	for k,v in pairs(map) do
		TuaMap[k] = self.baseTua
	end
end

function Aoc:InputAnt()
	local ant = table.Clone(Ant)
	self.antList[#self.antList+1] = ant
	ant.cur = startIndex
	ant.dest = endIndex
	ant.path[#ant.path+1] = startIndex
	self:FindPath(ant)
	self:UpdateTua(ant)
end

function Aoc:Search(start,tail)
	startIndex = start
	endIndex = tail
	for i = 1,self.antCount do
		self:InputAnt()
	end
	local bestAnt = self:CalBestPath()
	log:LogGroup("----------------------------ant search path ",bestAnt.path)

end

function Aoc:FindPath(ant)
	while true do
		---判断是否走到终点
		if ant.cur == ant.dest then
			break
		end
		local next = self:FindNextMaxTuaNode(ant.cur)
		ant.path[#ant.path + 1] = next
		ant.cur = next
		if #ant.path >= ant.maxStep then
			break
		end
	end
end

function Aoc:UpdateTua(ant)

	local dis = 0
	for i = 1,#ant.path - 1 do
		local next = NodePos[ant.path[i+1]]
		local cur = NodePos[ant.path[i]]
		dis =  dis + (cur - next).magnitude
	end
	local delta =  ant.tua/dis

	local info = {}
	for i = 1,#ant.path do
		local nodeIndex = ant.path[i]
		if info[nodeIndex] == nil then
			info[nodeIndex] = 0
		end
		info[nodeIndex] = info[nodeIndex] + 1
		TuaMap[nodeIndex] = (1 - self.rho) * TuaMap[nodeIndex] + delta
	end
	
end

function Aoc:CalBestPath()
	local maxAnt = nil
	for k,ant in pairs(self.antList) do
		local path = ant.path
		ant.totalTua = 0
		for _,nodeIndex in ipairs(path) do
			ant.totalTua = ant.totalTua + TuaMap[nodeIndex]
		end
		ant.totalTua = ant.totalTua/#path
		if not maxAnt then
			maxAnt = ant
		end
		if ant.totalTua > maxAnt.totalTua then
			maxAnt = ant
		end
	end
	return maxAnt
end

function Aoc:FindNextMaxTuaNode(curNode)
	local epos = NodePos[endIndex]
	local link = map[curNode]
	local per = {}
	local count = 0
	for i = 1,#link do
		local index = link[i]
		local checkPos = NodePos[index]
		local dis = (epos - checkPos).magnitude
		if dis == 0 then
			return index
		end
		count = count + TuaMap[index] * (1/dis)
	end 
	local cur = 0
	for i = 1,#link do
		local index = link[i]
		local checkPos = NodePos[index]
		local dis = (epos - checkPos).magnitude
		local add = TuaMap[index] * (1/dis)
		cur = cur + (add/count ) * 1000
		per[#per+1] = cur
	end 

	local rand = math.random(1,999)
	for index,value in ipairs(per) do
		if rand < value then
			return link[index]
		end
	end
end

function Aoc:Init()
	self:InitTuaMap()
end

function MainFunc()
	Aoc:Init()
	Aoc:Search(1,13)

end

MainFunc()

