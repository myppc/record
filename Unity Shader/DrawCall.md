
# DrawCall

![绘制流程](Picture/20201124151524888.png)

![绘制流程](Picture/20201126144950554.jpg)


每次cpu准备数据通知gpu的过程称为一个drawcall ,drawCall 只是发出绘制指令，drawcall本身并不会有太大开销
先测试可见性。cpu从硬盘加载渲染对象到内存再加载到显存。然后设置材质，纹理，顶点等信息。把图元传递给gpu进行渲染。
drawcall过多会造成cpu过载。

## 优化方案：
* 合批，打图集，少使用阴影之类的
* 条件：同层级，同Shader，同纹理

# unity有动态和静态批处理。
* 动态批处理有限制：一般900顶点数一下，取决于shader用到哪些参数。多Pass的shader，实时阴影等不能合批。缩放会影响合批。
物体可以移动
* 静态批处理需要标记静态，不能移动。同网格也会生成另一个网格数据传递给gpu，用内存换drawcall。

叠加会影响合批：
1. 最底层UI层级为0
2. 一个UI判断下面UI，可合批则层级相同，不可合批则该ui层级为下面ui层级+1
3. UI下有多个UI，则是所有层级判断结果里取最大值
造成两个图集里的贴图穿插叠加会造成多次drawcall。优化方案是尽量保证同图集的UI层级连续


# Batches 批处理
* Unity引擎开启批处理情况下将把满足条件的多个对象的渲染组合到一个内存块中以便减少由于资源切换而导致的 CPU 开销，也就是把多个DrawCall合并成一个DrawCall，来减少调用DrawCall的开销（主要是调用DrawCall之前的一系列设置），这个操作就是批处理。
* 把数据加载到显存，设置渲染状态及数据，CPU调用DrawCall的这一整个过程就是一个Batch。这个过程当中主要性能消耗点在于上传物体数据（加载到显存）和设置渲染状态及数据这一块，而不是DrawCall命令本身。
  
1. Batch流程：
   1. 设置顶点数据
   2. 设置shader程序（顶点shader，片元shader）
   3. 设置外部传入数据（各种变量，贴图等）
   4. 绘制物体

* 上图中一共有三个batch